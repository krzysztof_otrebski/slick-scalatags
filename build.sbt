enablePlugins(JavaAppPackaging)

name := "slick-scalatags"

version := "1.0"

scalaVersion := "2.12.6"

scalacOptions += "-P:clippy:colors=true"

libraryDependencies ++= {
  val akkaV = "2.5.12"
  val slickV = "3.2.3"
  val scalaTestV = "3.0.5"
  Seq(
    "com.typesafe.akka" %% "akka-actor" % akkaV,
    "com.typesafe.akka" %% "akka-stream" % akkaV,
    "com.typesafe.akka" %% "akka-http-core" % "10.1.1",
    "com.typesafe.akka" %% "akka-http-spray-json" % "10.1.1",
    "com.typesafe.akka" %% "akka-http-testkit" % "10.1.1",
    "com.typesafe.slick" %% "slick" % slickV,
    "com.github.pathikrit" %% "better-files" % "3.4.0",
    "com.typesafe.scala-logging" %% "scala-logging" % "3.5.0",
    "mysql" % "mysql-connector-java" % "5.1.38",
    "ch.qos.logback" % "logback-classic" % "1.1.7",
    "ch.qos.logback" % "logback-core" % "1.1.7",
    "com.lihaoyi" %% "scalatags" % "0.6.7",
    "org.scalatest" %% "scalatest" % scalaTestV % "test"
  )

}