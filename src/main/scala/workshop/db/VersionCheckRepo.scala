package workshop.db

import java.time.LocalDate

import workshop._

import scala.concurrent.Future

trait VersionCheckRepo {

  def create(): Future[Unit] = Future.successful()

  def add(versionChecks: VersionCheck*): Future[Seq[Int]]

  def get(id: Long): Future[Option[VersionCheck]]

  def countries(since: LocalDate, until: LocalDate): Future[Map[Country, Count]]

  def majorJava(since: LocalDate, until: LocalDate): Future[Map[JavaMajorVersion, Count]]

  def majorJavaThroughTime(since: LocalDate, until: LocalDate): Future[Map[StringDate, Map[JavaMajorVersion, Count]]]

  def majorJava(since: LocalDate, until: LocalDate, country: Country): Future[Map[JavaMajorVersion, Count]]

  def olvVersion(since: LocalDate, until: LocalDate): Future[Map[OlvVersion, Count]]
}
