package workshop.db

import java.time.LocalDate

import com.typesafe.scalalogging.LazyLogging
import slick.jdbc.JdbcBackend.Database
import slick.jdbc.JdbcProfile
import workshop._
import workshop.db.SlickVersionCheckRepo.Names

import scala.concurrent.{ExecutionContext, ExecutionContextExecutor, Future}
import scala.language.{implicitConversions, postfixOps}

class SlickVersionCheckRepo(val driver: JdbcProfile, val db: Database)
  extends VersionCheckRepo with LazyLogging {

  import driver.api._

  private implicit val ec: ExecutionContextExecutor = ExecutionContext.global
  val locations = TableQuery[Location]
  val javaVersions = TableQuery[JavaVersion]
  val versionChecks = TableQuery[VersionChecks]


  override def create(): Future[Unit] = {
    val schema = locations.schema ++ javaVersions.schema ++ versionChecks.schema
    db.run(schema.create)
  }


  private def generateLocationId(vc: VersionCheck): String = {
    (for {
      country <- vc.country.map(_.value)
      region <- vc.region.map(_.value)
      city <- vc.city.map(_.value)
    } yield s"$country/$region/$city").getOrElse("?/?/?")
  }

  private def generateJavaVersionId(vc: VersionCheck): String = {
    (for {
      version <- vc.javaVersion.map(_.value)
      vendor <- vc.vmVendor.map(_.value)
    } yield s"$version/$vendor").getOrElse("?/?")
  }


  override def add(vcs: workshop.VersionCheck*): Future[Seq[Int]] = ???


  override def get(id: Long): Future[Option[VersionCheck]] = ???

  override def countries(since: LocalDate, until: LocalDate): Future[Map[workshop.Country, workshop.Count]] = ???

  override def majorJava(since: LocalDate, until: LocalDate): Future[Map[workshop.JavaMajorVersion, workshop.Count]] = ???

  override def majorJava(since: LocalDate, until: LocalDate, country: workshop.Country): Future[Map[workshop.JavaMajorVersion, workshop.Count]] = ???

  override def majorJavaThroughTime(since: LocalDate, until: LocalDate): Future[Map[StringDate, Map[JavaMajorVersion, Count]]] = ???

  override def olvVersion(since: LocalDate, until: LocalDate): Future[Map[workshop.OlvVersion, workshop.Count]] = ???


  class Location(tag: Tag) extends Table[(String, Country, Region, City, CityLong, CityLat)](tag, Names.Location) {
    def id = column[String]("id", O.PrimaryKey, O.Length(60))

    def country = column[Country]("Country", O.Length(40))

    def region = column[Region]("Region", O.Length(40))

    def city = column[City]("City", O.Length(40))

    def long = column[CityLong]("Long")

    def lat = column[CityLat]("Lat")

    override def * = (id, country, region, city, long, lat)
  }

  //TODO define full model
  class JavaVersion(tag: Tag) extends Table[(String)](tag, Names.JavaVersion) {

    def id = column[String]("id", O.PrimaryKey, O.Length(60))

    override def * = (id)

  }

  //TODO define full model
  class VersionChecks(tag: Tag) extends Table[(Long)](tag, Names.VersionChecks) {

    def id = column[Long]("id", O.PrimaryKey)

    override def * = (id)

  }


}

object SlickVersionCheckRepo {

  implicit def dateConvertJtoS(date: LocalDate): java.sql.Date = java.sql.Date.valueOf(date)

  implicit def dateConvertStoJ(date: java.sql.Date): LocalDate = date.toLocalDate

  object Names {
    val Location = "Location"
    val JavaVersion = "JavaVersion"
    val VersionChecks = "VersionChecks"
  }

}
