package workshop.db

import java.time.LocalDate
import java.util.Date

import akka.actor.ActorSystem
import workshop._

import scala.concurrent.{ExecutionContext, ExecutionContextExecutor, Future, Promise}
import scala.language.postfixOps
import scala.util.{Success, Try}

class FakeVersionCheckRepo(actorSystem: ActorSystem) extends VersionCheckRepo {

  override def add(versionChecks: workshop.VersionCheck*): Future[Seq[Int]] = {
    import scala.concurrent.duration._
    implicit val ec: ExecutionContextExecutor = ExecutionContext.global
    val p = Promise[Seq[Int]]()
    val r: Try[Seq[Int]] = Success(Seq(1))
    actorSystem.scheduler.scheduleOnce(delay = 30 millis, runnable = new Runnable {
      override def run(): Unit = p.complete(r)
    })
    p.future
  }

  override def get(id: Long): Future[Option[VersionCheck]] = Future.successful(
    Some(VersionCheck(
      id = id,
      checkDate = new Date(213123123123L),
      uuid = Some(UserId("A")),
      clientVersion = OlvVersion("1.2.2"),
      country = Some(Country("PL")),
      region = Some(Region("malopolska")),
      city = Some(City("Krakow")),
      cityLong = Some(CityLong(10)),
      cityLat = Some(CityLat(4)),
      osName = Some(Os("Linux")),
      javaVersion = Some(JavaFullVersion("1.8.53")),
      vmVendor = Some(JavaVendor("Oracle"))
    )
    )
  )

  override def countries(since: LocalDate, until: LocalDate): Future[Map[Country, Count]] = Future.successful(
    Map(
      Country("PL") -> Count(10),
      Country("MA") -> Count(20),
      Country("GB") -> Count(60),
      Country("US") -> Count(40),
      Country("IN") -> Count(50),
    )
  )

  override def majorJava(since: LocalDate, until: LocalDate): Future[Map[JavaMajorVersion, Count]] = Future.successful(
    Map(
      JavaMajorVersion("6") -> Count(20),
      JavaMajorVersion("7") -> Count(30),
      JavaMajorVersion("8") -> Count(60),
      JavaMajorVersion("9") -> Count(30),
      JavaMajorVersion("10") -> Count(20),
      JavaMajorVersion("?") -> Count(20),
    )
  )

  override def majorJava(since: LocalDate, until: LocalDate, country: Country): Future[Map[JavaMajorVersion, Count]] = Future.successful(
    Map(
      JavaMajorVersion("6") -> Count(20),
      JavaMajorVersion("7") -> Count(30),
      JavaMajorVersion("8") -> Count(60),
      JavaMajorVersion("9") -> Count(30),
      JavaMajorVersion("10") -> Count(20),
      JavaMajorVersion("?") -> Count(20),
    )
  )

  override def olvVersion(since: LocalDate, until: LocalDate): Future[Map[OlvVersion, Count]] = Future.successful(
    Map(
      OlvVersion("6") -> Count(20),
      OlvVersion("7") -> Count(30),
      OlvVersion("8") -> Count(60),
      OlvVersion("9") -> Count(30),
      OlvVersion("10") -> Count(20),
      OlvVersion("?") -> Count(20),
    )
  )

  override def majorJavaThroughTime(since: LocalDate, until: LocalDate): Future[Map[StringDate, Map[JavaMajorVersion, Count]]] = Future.successful(
    Map(
      StringDate("2015") -> Map(
        JavaMajorVersion("6") -> Count(20),
        JavaMajorVersion("7") -> Count(30),
        JavaMajorVersion("8") -> Count(60),
      ),
      StringDate("2016") -> Map(
        JavaMajorVersion("6") -> Count(14),
        JavaMajorVersion("7") -> Count(36),
        JavaMajorVersion("8") -> Count(69),
      ),
      StringDate("2017") -> Map(
        JavaMajorVersion("6") -> Count(4),
        JavaMajorVersion("7") -> Count(16),
        JavaMajorVersion("8") -> Count(99),
      )
    )
  )
}
