package workshop.json

import java.util.Date

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json._
import workshop.{UserId, _}

object VersionCheckV1Protocol extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val dateFormat: DateMarshalling.DateFormat.type = DateMarshalling.DateFormat
  implicit val geoFormat: RootJsonFormat[GeoV1] = jsonFormat5(GeoV1.apply)
  implicit val envFormat: RootJsonFormat[EnvV1] = jsonFormat3(EnvV1.apply)
  implicit val versionCheckFormat: RootJsonFormat[VersionCheckV1] = jsonFormat6(VersionCheckV1.apply)
}

case class VersionCheckV1(id: Long,
                          checkDate: Date,
                          uuid: Option[String],
                          clientVersion: String,
                          geo: Option[GeoV1] = None,
                          env: Option[EnvV1] = None) {
  def toDomain: VersionCheck = {
    val maybeJavaOs = env.map { j =>
      if (j.javaVersion.toLowerCase.matches(".*(win|mac|linux|free).*")) {
        (j.osName, j.javaVersion)
      } else {
        (j.javaVersion, j.osName)
      }
    }

    val java: Option[String] = maybeJavaOs.map(_._1)
    val osName: Option[String] = maybeJavaOs.map(_._2)

    VersionCheck(
      id = id,
      checkDate = checkDate,
      uuid = uuid.map(UserId),
      clientVersion = OlvVersion(clientVersion),
      country = geo.map(_.country).map(Country),
      region = geo.map(_.region).map(Region),
      city = geo.map(_.city).map(City),
      cityLong = geo.map(_.cityLong).map(CityLong),
      cityLat = geo.map(_.cityLat).map(CityLat),
      osName = osName.map(Os),
      javaVersion = java.map(j => JavaFullVersion(j)),
      vmVendor = env.map(e => JavaVendor(e.vmVendor))
    )
  }

}

object VersionCheckV1 {
  def toApi(vc: VersionCheck): VersionCheckV1 = {

    val geo: Option[GeoV1] = for {
      country <- vc.country
      region <- vc.region
      city <- vc.city
      cityLong <- vc.cityLong
      cityLat <- vc.cityLat
    } yield GeoV1(
      region = region.value,
      city = city.value,
      country = country.value,
      cityLat = cityLat.value,
      cityLong = cityLong.value
    )

    val env = for {
      osName <- vc.osName
      javaVersion <- vc.javaVersion
      vmVendor <- vc.vmVendor
    } yield EnvV1(
      osName = osName.value,
      javaVersion = javaVersion.value,
      vmVendor = vmVendor.value
    )

    VersionCheckV1(
      id = vc.id,
      checkDate = vc.checkDate,
      uuid = vc.uuid.map(_.value),
      clientVersion = vc.clientVersion.value,
      geo = geo,
      env = env
    )
  }
}

case class GeoV1(region: String, cityLong: Double, cityLat: Double, country: String, city: String)

case class EnvV1(osName: String, javaVersion: String, vmVendor: String)
