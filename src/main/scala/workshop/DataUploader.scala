package workshop

import akka.actor.ActorSystem
import akka.dispatch.ExecutionContexts
import better.files._
import slick.jdbc.{JdbcBackend, MySQLProfile}
import workshop.db.SlickVersionCheckRepo
import workshop.json.VersionCheckV1

import scala.concurrent.{Await, ExecutionContextExecutor}
import scala.language.postfixOps

object DataUploader extends App {

  import slick.jdbc.JdbcBackend.Database
  import spray.json._
  import workshop.json.VersionCheckV1Protocol._

  val db: JdbcBackend.Database = Database.forConfig("mysqlDB")
  private val system = ActorSystem("data-uploader")
  val repo = new SlickVersionCheckRepo(MySQLProfile, db)


  import scala.concurrent.duration._

  implicit val ec: ExecutionContextExecutor = ExecutionContexts.global()

  Await.result(repo.create(), 1 minute)


  private val files = file"data"
    .list
    .filter(p => p.name.endsWith("json.gz"))
    .toList

  files
    .map(p => p.newInputStream.asGzipInputStream().lines.mkString("\n"))
    .map(_.parseJson)
    .map(_.convertTo[List[VersionCheckV1]])
    .map(_.map(_.toDomain))
    .zipWithIndex
    .foreach {
      case (versionChecks, index) => addToDb(index + 1, files.size, versionChecks)
    }
  println("Done")
  system.terminate()

  def addToDb(currentFile: Int, filesCount: Int, vcs: List[VersionCheck]): Unit = {
    print(s"Adding ${vcs.length} events")
    val chunkSize = math.max(10, vcs.size / 30)
    val chunks = vcs.grouped(chunkSize).toList
    chunks.zipWithIndex.foreach {
      case (chunk, index) =>
        val start = System.currentTimeMillis()
        val value = repo.add(chunk: _*)
        Await.result(value, 10 minutes)
        val duration = System.currentTimeMillis() - start
        val p =
          s"""\rFile $currentFile of $filesCount [${"*" * (index + 1)}${"-" * (chunks.size - (index + 1))}] [${1000 * chunk.length.toFloat / duration} events/s]   """
        print(p)
    }
    println()

  }
}
