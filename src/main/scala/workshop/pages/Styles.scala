package workshop.pages

import scalatags.Text.all.{backgroundColor, _}
import scalatags.stylesheet.{Cls, StyleSheet}

object Styles extends StyleSheet {

  initStyleSheet()

  val h1: Cls = cls(
    backgroundColor := "#33AA33",
    color := "#d4d4d4",
    padding := "5px",
    margin := "5px",
    border := "2px solid darkblue",
    borderRadius := "5px"
  )
}
