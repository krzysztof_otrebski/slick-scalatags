package workshop.pages

import scalatags.Text

object MainPage {

  def render(): Text.TypedTag[String] = {
    import scalatags.Text.all._
    html(
      head(link(rel := "stylesheet", href := "styles.css")),
      body(
        h1(Styles.h1, "Hello"),
        a(href := "/get/1", "Single event"), br,
        a(href := "olv-versions?since=2016-10&until=2018-10", "OLV version"), br,
        a(href := "java-versions?since=2016-10&until=2018-10", "Java version"), br,
        a(href := "country?since=2016-10&until=2018-10", "Countries"), br
      )
    )
  }
}
