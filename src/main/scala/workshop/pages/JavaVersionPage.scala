package workshop.pages

import scalatags.Text
import workshop.{Count, JavaMajorVersion, StringDate}

class JavaVersionPage(versions: Map[JavaMajorVersion, Count], times: Map[StringDate, Map[JavaMajorVersion, Count]] = Map.empty) {

  def render(): String = {
    versions
      .toList
      .sortBy(_._1.value)
      .map {
        case (JavaMajorVersion(v), Count(count)) => s""" $v|$count """
      }.mkString("Version  Count\n", "\n", "")

  }

  def renderHtml(): Text.TypedTag[String] = {
    import scalatags.Text.all._
    html(
      head(
        script(src := Charts.ScripUrl),
        script(Charts.javaPieChart(versions, "chart")),
        script(Charts.javaVersionTimeSeriesChart(times, "timeseries"))
      ),
      body(
        h1("Chart"),
        div(id := "chart"),
        div(id := "timeseries")
      )
    )
  }
}
