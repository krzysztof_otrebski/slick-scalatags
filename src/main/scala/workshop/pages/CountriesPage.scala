package workshop.pages

import java.time.LocalDate
import java.util.Locale

import workshop.{Count, Country}

class CountriesPage(since: LocalDate, until: LocalDate
                    , map: Map[Country, Count]) {

  def render(): String = {

    val data = map
      .map {
        case (Country(code), Count(count)) => s"${countryName(code)}      | $count"
      }.mkString("\n")
    s"""Since: ${since.toString}
       |Until: ${until.toString}
       |Country   | Count
       |$data
     """.stripMargin
  }

  def countryName(code: String): String = {
    if (code.length == 2)
      new Locale("", code).getDisplayCountry(Locale.ENGLISH)
    else {
      "?"
    }
  }
}
