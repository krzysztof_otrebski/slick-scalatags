package workshop.pages

import workshop.{Count, JavaMajorVersion, Os, StringDate}

import scala.collection.immutable

object Charts {

  val ScripUrl = "https://www.gstatic.com/charts/loader.js"

  def javaPieChart(values: Map[JavaMajorVersion, Count], tagId: String): String = {
    val data = values.map {
      case (JavaMajorVersion(version), Count(count)) => version -> count
    }
    pieChart("Java version", data, tagId)
  }

  def osPieChart(values: Map[Os, Count], tagId: String): String = {
    val data = values.map {
      case (Os(os), Count(count)) => os -> count
    }
    pieChart("OS", data, tagId)
  }

  private def pieChart(property: String, values: Map[String, Int], tagId: String): String = {
    val d = values
      .toList
      .sortBy(_._2)
      .map {
        case (version, count) => s"['$version', $count]"
      }.mkString(s"['$property', 'Count'],\n", ",\n", "")

    val totalChartJs =
      s"""
         |      google.charts.load('current', {packages:['corechart']});
         |      google.charts.setOnLoadCallback(drawChart);
         |      function drawChart() {
         |        var data = google.visualization.arrayToDataTable([
         |          $d
         |        ]);
         |
         |        var options = {
         |          title: 'Java versions',
         |          pieHole: 0.4,
         |          is3D: true,
         |
         |        };
         |
         |        var chart = new google.visualization.PieChart(document.getElementById('$tagId'));
         |        chart.draw(data, options);
         |      }
       """.stripMargin
    totalChartJs
  }

  def javaVersionTimeSeriesChart(v: Map[StringDate, Map[JavaMajorVersion, Count]], tag: String): String = {
    val x = v.mapValues(_.map { case (JavaMajorVersion(version), Count(count)) => version -> count })
    timeSeriesChart("Java version", x, tag)

  }

  def timeSeriesChart(title: String, v: Map[StringDate, Map[String, Int]], tag: String): String = {

    val keys: Seq[String] =  v.flatMap(_._2.keySet).toSet.toList.sorted

    val rows: immutable.Seq[String] = v.toList.sortBy(_._1.date)
      .map {
        case (StringDate(date), values: Map[String, Int]) =>
          val data = keys.map(values.getOrElse(_, 0)).mkString(",")
          s"""['$date', $data]"""
      }

    val header = s"""['$title', ${keys.map(k => s"'$k'").mkString(", ")}]"""

    s"""
       |google.charts.load('current', {'packages':['corechart']});
       |      google.charts.setOnLoadCallback(drawChart);
       |
       |      function drawChart() {
       |        var data = google.visualization.arrayToDataTable([
       |          $header,
       |          ${rows.mkString(",\n")}
       |        ]);
       |
       |        var options = {
       |          title: '$title',
       |          curveType: 'function',
       |          legend: { position: 'bottom' }
       |        };
       |
       |        var chart = new google.visualization.LineChart(document.getElementById('$tag'));
       |
       |        chart.draw(data, options);
       |      }
       """.stripMargin
  }

}
