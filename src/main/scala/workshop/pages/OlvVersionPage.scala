package workshop.pages

import java.time.LocalDate

import workshop.{Count, OlvVersion}

class OlvVersionPage(since: LocalDate, until: LocalDate, versions: Map[OlvVersion, Count]) {

  def render(): String = {
    val data = versions
      .map {
        case (OlvVersion(c), Count(cc)) => s"$c      | $cc"
      }.mkString("\n")
    s"""Since: ${since.toString}
       |Until: ${until.toString}
       |OlvVersion   | Count
       |$data
     """.stripMargin
  }

}
