package workshop.web

import java.time.LocalDate

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshalling.ToResponseMarshallable
import akka.http.scaladsl.model.{ContentTypes, HttpEntity}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer
import com.typesafe.scalalogging.LazyLogging
import scalatags.Text
import workshop.db.VersionCheckRepo
import workshop.json.VersionCheckV1
import workshop.pages._

import scala.concurrent.{ExecutionContextExecutor, Future}
import scala.io.StdIn
import scala.language.{implicitConversions, postfixOps}

class WebServer(repo: VersionCheckRepo)(implicit val system: ActorSystem) extends LazyLogging {
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val executionContext: ExecutionContextExecutor = system.dispatcher

  import workshop.json.VersionCheckV1Protocol._

  implicit def pageConverter(page: Text.TypedTag[String]): ToResponseMarshallable = {
    ToResponseMarshallable[HttpEntity.Strict](HttpEntity(ContentTypes.`text/html(UTF-8)`, page.render))
  }

  implicit def pageConverter(page: Future[Text.TypedTag[String]]): ToResponseMarshallable = {
    page.map(p => ToResponseMarshallable[HttpEntity.Strict](HttpEntity(ContentTypes.`text/html(UTF-8)`, p.render)))
  }


  val route: Route = path("") {
    complete(MainPage.render())
  } ~ path("get" / LongNumber) { id =>
    complete(repo.get(id).map(_.map(VersionCheckV1.toApi)))
  } ~ parameterMap { params =>
    val since = Query.since(params)
    val until = Query.until(params)

    path("olv-versions") {
      complete(repo.olvVersion(since, until).map(new OlvVersionPage(since, until, _).render()))
    } ~ path("java-versions") {
      val page = for {
        f1 <- repo.majorJava(since, until)
        f2 <- repo.majorJavaThroughTime(since, until)
      } yield new JavaVersionPage(f1, f2)
      complete(page.map(_.renderHtml()))
    } ~ path("country") {
      complete(repo.countries(since, until).map(new CountriesPage(since, until, _).render()))
    } ~ path("styles.css") {
      complete(Styles.styleSheetText)
    }
  } ~ path ("flag"/ Segment) { name =>
    getFromResource(s"flags/${name.toLowerCase}.png")
  }

  Http().bindAndHandle(route, "0.0.0.0", 8080)
  println("Started, press enter to quit")
  StdIn.readLine()
  system.terminate()


  object Query {
    def parseDateString(s: String): Option[LocalDate] = {
      val year = """(\d{4})""".r
      val yearMonth = """(\d{4})-(\d{1,2})""".r
      val yearMonthDay = """(\d{4})-(\d{1,2})-(\d{1,2})""".r

      s match {
        case year(y) => Some(LocalDate.of(y.toInt, 1, 1))
        case yearMonth(y, m) => Some(LocalDate.of(y.toInt, m.toInt, 1))
        case yearMonthDay(y, m, d) => Some(LocalDate.of(y.toInt, m.toInt, d.toInt))
        case _ => None
      }
    }

    def since(map: Map[String, String]): LocalDate = map
      .get("since")
      .flatMap(parseDateString)
      .getOrElse(LocalDate.of(1970, 1, 1))

    def until(map: Map[String, String]): LocalDate = map
      .get("until")
      .flatMap(parseDateString)
      .getOrElse(LocalDate.now())

  }

}
