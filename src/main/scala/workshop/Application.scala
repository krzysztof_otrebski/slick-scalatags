package workshop

import akka.actor.ActorSystem
import workshop.db.FakeVersionCheckRepo
import workshop.web.WebServer

object Application extends App {

  println("Starting...")

  private implicit val systemSystem: ActorSystem = ActorSystem("system")
  new WebServer(repo = new FakeVersionCheckRepo(systemSystem))
}
