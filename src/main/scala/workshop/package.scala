import java.util.Date

import slick.lifted.MappedTo

package object workshop {

  case class VersionCheck(id: Long,
                          checkDate: Date,
                          uuid: Option[UserId],
                          clientVersion: OlvVersion,
                          country: Option[Country],
                          region: Option[Region],
                          city: Option[City],
                          cityLong: Option[CityLong],
                          cityLat: Option[CityLat],
                          osName: Option[Os],
                          javaVersion: Option[JavaFullVersion],
                          vmVendor: Option[JavaVendor])


  case class JavaFullVersion(value: String) extends MappedTo[String] {
    def majorJava(): JavaMajorVersion = {
      val majorVersion = if (value.startsWith("1.")) {
        value.split("\\.")(1)
      } else if (value.matches("\\d+")) {
        value
      } else if (value.matches("\\d+\\.")) {
        value.split("\\.").head
      } else {
        "?"
      }
      JavaMajorVersion(majorVersion)
    }
  }

  case class JavaMajorVersion(value: String) extends MappedTo[String]

  case class JavaVendor(value: String) extends MappedTo[String]

  case class Country(value: String) extends MappedTo[String]

  case class Region(value: String) extends MappedTo[String]

  case class City(value: String) extends MappedTo[String]

  case class CityLong(value: Double) extends MappedTo[Double]

  case class CityLat(value: Double) extends MappedTo[Double]

  case class DateString(date: String)

  case class Count(count: Int)

  case class UserId(value: String) extends MappedTo[String]

  case class Ip(value: String) extends MappedTo[String]

  case class OlvVersion(value: String) extends MappedTo[String]

  case class Os(value: String) extends MappedTo[String]

  case class StringDate(date: String)

}
